import random


class Brackets:
    @staticmethod
    def gen_brackets(length):
        if length == 0:
            return ''
        if length == 1:
            return '()'
        if random.random() > 0.25:  # value computed experimentally
            return '(' + Brackets.gen_brackets(length - 1) + ')'

        split_point = random.choice(list(range(1, length)))
        return Brackets.gen_brackets(split_point) + Brackets.gen_brackets(length - split_point)

    @staticmethod
    def gen_brackets1(depth):
        if depth == 0:
            return ''
        if depth == 1:
            return '()' * random.choice(range(1, 4))

        core = '(' + Brackets.gen_brackets1(depth - 1) + ')'
        brackets = [Brackets.gen_brackets1(l) for l in random.choices(range(depth // 2), k=1)]
        all_brackets = [core, *brackets]
        random.shuffle(all_brackets)
        return ''.join(all_brackets)

    @staticmethod
    def gen_brackets2(consecutive):
        slope = '(' * consecutive + ')'

        brackets = [Brackets.gen_brackets1(l) for l in random.choices(range(consecutive), k=2)]
        all_brackets = [slope, *brackets]
        random.shuffle(all_brackets)

        end = ')' * random.choice(range(max(consecutive // 2, 1)))
        begin = '(' * len(end) + '()'
        return begin + ''.join(all_brackets) + ')' * (consecutive - 1) + end

    @staticmethod
    def gen_brackets3(distance):
        core = '(' + Brackets.gen_brackets(distance // 2) + ')'
        brackets = [Brackets.gen_brackets(l) for l in random.choices(range(max(distance // 2, 1)), k=3)]

        all_brackets = [core, *brackets]
        random.shuffle(all_brackets)

        return ''.join(all_brackets)

    @staticmethod
    def one_hot_brackets(brackets):
        return [[1, 0] if c == '(' else [0, 1] for c in brackets]

    @staticmethod
    def unhot_brackets(brackets):
        return ''.join(['(' if bracket[0] else ')' for bracket in brackets])

    @staticmethod
    def is_valid(brackets):
        curr = 0
        for c in brackets:
            if c == '(':
                curr += 1
            elif c == ')':
                curr -= 1
                if curr < 0:
                    return False
            else:
                return False
        return brackets and (curr == 0)

    @staticmethod
    def opened_brackets(brackets):
        best, curr = 0, 0
        for c in brackets:
            if c == '(':
                curr += 1
                best = max(best, curr)
            else:
                curr -= 1
        return best

    @staticmethod
    def consecutive_brackets(brackets):
        best, curr = 0, 0
        for c in brackets:
            if c == '(':
                curr += 1
                best = max(best, curr)
            else:
                curr = 0
        return best

    @staticmethod
    def farest_brackets(brackets):
        best, curr, level = 0, 0, 0
        for c in brackets:
            if c == '(':
                level += 1
            else:
                level -= 1

            if level == 0:
                best = max(curr, best)
                curr = 0
            else:
                curr += 1

        return best

    @staticmethod
    def result_brackets(brackets):
        return [
            Brackets.opened_brackets(brackets),
            Brackets.consecutive_brackets(brackets),
            Brackets.farest_brackets(brackets)
        ]
