import datetime
from brackets import Brackets

import numpy as np
import tensorflow as tf
import tensorflow.contrib.rnn as tf_rnn
import matplotlib.pyplot as plt


class BracketRNN:
    def __init__(self, batch_size, state_size, cell_type):
        self.sess = None

        self.batch_size = batch_size
        self.state_size = state_size

        self.x, self.y = self.build_placeholders()
        self.truncate = tf.cast(tf.shape(self.x)[1], tf.float64)
        self.cell = self.build_rnn(cell_type)
        self.W1, self.W2, self.b = self.build_predictions()

        _, self.final_state = tf.nn.dynamic_rnn(self.cell, self.x, dtype=tf.float64)
        self.result = self.predict(cell_type)
        self.clean_result = tf.round(self.result)

        self.loss = self.loss_fun()
        self.accuracy = self.accuracy_fun()
        self.diff = self.diff_fun()
        self.train_step = tf.train.AdamOptimizer().minimize(self.loss)

        self.saver = tf.train.Saver()

    def build_rnn(self, cell_type):
        if cell_type == 'rnn':
            return tf_rnn.BasicRNNCell(self.state_size, activation=tf.nn.leaky_relu)
        elif cell_type == 'lstm':
            return tf_rnn.BasicLSTMCell(self.state_size, activation=tf.nn.leaky_relu)

    def build_placeholders(self):
        return tf.placeholder(tf.float64, (self.batch_size, None, 2), name='x'), \
               tf.placeholder(tf.float64, (self.batch_size, 3), name='y')

    def build_predictions(self):
        with tf.variable_scope('predict_y'):
            return tf.get_variable('W1', [self.state_size, 3], initializer=tf.constant_initializer(0), dtype=tf.float64), \
                   tf.get_variable('W2', [self.state_size, 3], initializer=tf.constant_initializer(0), dtype=tf.float64), \
                   tf.get_variable('b', [3], initializer=tf.constant_initializer(0), dtype=tf.float64)

    def predict(self, cell_type):
        if cell_type == 'rnn':
            return tf.matmul(self.final_state, self.W1) + self.b
        elif cell_type == 'lstm':
            return tf.matmul(self.final_state[0], self.W1) + tf.matmul(self.final_state[1], self.W2) + self.b

    def loss_fun(self):
        return tf.losses.mean_squared_error(labels=self.y, predictions=self.result, weights=4)

    def accuracy_fun(self):
        return [tf.reduce_mean(tf.cast(tf.equal(self.y[:, i], self.clean_result[:, i]), tf.float64)) for i in range(3)]

    def diff_fun(self):
        return [tf.reduce_mean(tf.abs(self.y[:, i] - self.clean_result[:, i])) for i in range(3)]

    def generate_feed_dict(self, lower, upper, generator=Brackets.gen_brackets):
        truncated_size = np.random.randint(lower, upper)
        brackets = [generator(truncated_size) for _ in range(self.batch_size)]

        return {
            self.x: [Brackets.one_hot_brackets(b) for b in brackets],
            self.y: [Brackets.result_brackets(b) for b in brackets]
        }

    def train(self, batch, length_limit, info, filename, boost):
        with tf.Session() as self.sess:
            self.sess.run(tf.global_variables_initializer())

            lower, upper = 1, 20
            accuracies, losses = [], []
            for i in range(1, batch+1):
                feed_dict = self.generate_feed_dict(lower, upper)
                accuracy, loss, _ = self.sess.run([self.accuracy, self.loss, self.train_step],
                                                  feed_dict=feed_dict)
                accuracies.append(accuracy)
                losses.append(loss)

                if i % info == 0:
                    print(datetime.datetime.utcnow(), flush=True)
                    print('Iter: {}'.format(i), flush=True)

                    mean_loss = np.mean(losses[-info:])
                    print('Loss: {}'.format(mean_loss), flush=True)

                    mean_accuracies = [np.mean(accuracies) for accuracies in zip(*accuracies[-info:])]
                    print('Accuracies: {}'.format(mean_accuracies), flush=True)

                    if all([mean >= boost for mean in mean_accuracies]):
                        upper += 5
                        print('Boost truncate size to ({}, {})'.format(lower, upper), flush=True)
                        if upper >= length_limit:
                            break

            self.saver.save(self.sess, filename)

    def statistics(self, length, repeat, filename, generator=Brackets.gen_brackets):
        with tf.Session() as self.sess:
            self.saver.restore(self.sess, filename)

            for length in range(1, length+1):
                accuracies, losses, diffs = [], [], []

                for _ in range(repeat):
                    feed_dict = self.generate_feed_dict(length, length+1, generator)
                    accuracy, loss, diff = self.sess.run([self.accuracy, self.loss, self.diff],
                                                         feed_dict=feed_dict)

                    accuracies.append(accuracy)
                    losses.append(loss)
                    diffs.append(diff)

                yield {
                    'length': length,
                    'loss': np.mean(losses),
                    'accuracy': np.mean(accuracies, 0),
                    'diff': np.mean(diffs, 0),
                }

    def draw_plots(self, cell_type, cell_filename):
        little, big = 20, 100
        tests = 500

        generic_stats = list(self.statistics(big, tests, cell_filename, Brackets.gen_brackets))

        for i in range(3):
            plt.plot(list(range(2, 2 * big + 1, 2)), [x['accuracy'][i] for x in generic_stats])
            plt.title('{} accuracy for value {}.'.format(cell_type, i + 1))
            plt.xlabel('Length of sequence')
            plt.ylabel('Accuracy')
            plt.grid(True, axis='y')
            plt.savefig('plots/{}_accuracy_{}.png'.format(cell_type, i + 1))
            plt.clf()

        plt.plot(list(range(2, 2 * big + 1, 2)), [x['loss'] for x in generic_stats])
        plt.title('{} mean loss.'.format(cell_type))
        plt.xlabel('Length of sequence')
        plt.ylabel('Loss')
        plt.grid(True, axis='y')
        plt.savefig('plots/{}_loss.png'.format(cell_type))
        plt.clf()

        for i in range(3):
            plt.plot(list(range(2, 2 * big + 1, 2)), [x['diff'][i] for x in generic_stats])
            plt.title('{} difference for value {}.'.format(cell_type, i + 1))
            plt.xlabel('Length of sequence')
            plt.ylabel('Difference')
            plt.grid(True, axis='y')
            plt.savefig('plots/{}_difference_{}.png'.format(cell_type, i + 1))
            plt.clf()

        sizes = [little, little, big]
        generators = [Brackets.gen_brackets1, Brackets.gen_brackets2, Brackets.gen_brackets3]
        getter = [(lambda x, id: x[id]), (lambda x, id: x), (lambda x, id: x[id])]
        values = ['accuracy', 'loss', 'diff']
        ylabels = ['Accuracy', 'Loss', 'Difference']
        for i in range(3):
            stats = list(self.statistics(sizes[i], 100, cell_filename, generators[i]))

            for j in range(3):
                plt.plot(list(range(1, sizes[i] + 1)), [getter[j](x[values[j]], i) for x in stats])
                plt.title('{} {} for value {} by ground truth.'.format(cell_type, values[j], i + 1))
                plt.xlabel('Ground truth')
                plt.ylabel(ylabels[j])
                plt.grid(True, axis='y')
                plt.savefig('plots/{}_ground_{}_{}.png'.format(cell_type, values[j], i + 1))
                plt.clf()

    def user_test(self, filename):
        with tf.Session() as self.sess:
            self.saver.restore(self.sess, filename)

            while True:
                test_brackets = input('Input bracket sequence to evaluate:')
                if not Brackets.is_valid(test_brackets):
                    print('Input sequence is invalid.', flush=True)
                    continue

                result = self.sess.run(self.clean_result, feed_dict={
                    self.x: [Brackets.one_hot_brackets(test_brackets) for _ in range(self.batch_size)]
                })
                print('Correct values: {}'.format(Brackets.result_brackets(test_brackets)), flush=True)
                print('Network result: {}'.format(list([int(x) for x in result[0]])), flush=True)


if __name__ == '__main__':
    cell_type = ['rnn', 'lstm'][int(input('0. RNN\n1. LSTM\n'))]
    cell_filename = 'saves/{}_save'.format(cell_type)

    action = int(input('0. Train\n1. Statistics\n2. User Input\n'))

    if action == 0:
        rnn = BracketRNN(batch_size=32, state_size=32, cell_type=cell_type)
        rnn.train(batch=800*1000, length_limit=80, info=1000, boost=0.6 if cell_type == 'rnn' else 0.7, filename=cell_filename)

    if action == 1:
        rnn = BracketRNN(batch_size=1, state_size=32, cell_type=cell_type)
        rnn.draw_plots(cell_type, cell_filename)

    if action == 2:
        rnn = BracketRNN(batch_size=1, state_size=32, cell_type=cell_type)
        rnn.user_test(filename=cell_filename)
