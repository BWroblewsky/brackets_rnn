### RNN brackets

Implementation of RNN and LSTM, designed to parsing sequence of brackets and predicting:
- maximum bracket level
- maximum consequtive opening brackets
- highest distance between opening and closing bracket

### Results 

Recurrent neural networks are not designed for such tasks - which was proved by tests.

Full report (in polish) below.

![alt text](glebokie-sieci-neuronowe-1.jpg)
![alt text](glebokie-sieci-neuronowe-2.jpg)
![alt text](glebokie-sieci-neuronowe-3.jpg)
![alt text](glebokie-sieci-neuronowe-4.jpg)
